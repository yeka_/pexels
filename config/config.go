package config

import (
	"fmt"
	"github.com/spf13/viper"
)

var defaultConfig *viper.Viper

func init() {
	defaultConfig = readConfig()
}

func Config() *viper.Viper {
	return defaultConfig
}

func readConfig() *viper.Viper {
	v := viper.New()
	v.SetConfigName("env")
	v.AddConfigPath(".")
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	} else  {
		fmt.Println("init config success.")
	}
	return v
}