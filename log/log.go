package log

import (
	"fmt"
	"go.uber.org/zap"
)

var defaultLogger *zap.Logger
var sugaredLogger *zap.SugaredLogger //  sugaredLogger支持以printf风格打日志

func init() {
	defaultLogger = initLogger()
	sugaredLogger = defaultLogger.Sugar()
	defaultLogger.Info("init logger success.")
}

func initLogger() *zap.Logger {
	// todo 配置生产环境和开发环境
	logger, err := zap.NewDevelopment()
	if err != nil {
		panic(fmt.Errorf("init logger fatal: %s", err))
	}
	return logger
}

func Log() *zap.Logger {
	return defaultLogger
}

func Slog() *zap.SugaredLogger {
	return sugaredLogger
}

