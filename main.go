package main

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/yeka_/pexels/service"
	"gitlab.com/yeka_/pexels/service/middleware"
	"log"
)

func main() {
	r := service.NewServer()
	log.Fatal(fasthttp.ListenAndServe(":8888", middleware.GlobalMiddleware(r.Router.Handler)))
}
