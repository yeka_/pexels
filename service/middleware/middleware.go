package middleware

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/yeka_/pexels/config"
	. "gitlab.com/yeka_/pexels/log"
	"go.uber.org/zap"
	"time"
)

// Middleware 局部中间件在此注册
func PartialMiddleware(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	if !config.Config().GetBool("PartialMiddleware") {
		return next
	}
	return  func(ctx *fasthttp.RequestCtx) {
		next(ctx)
	}
}

// GlobalMiddleware 全部中间件在此注册
func GlobalMiddleware(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	if !config.Config().GetBool("PartialMiddleware") {
		return next
	}
	return  func(ctx *fasthttp.RequestCtx) {
		t := time.Now()
		next(ctx)
		Log().Debug("next", zap.Duration("cost", time.Since(t)))

	}
}
