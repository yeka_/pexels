package service

import (

	"fmt"
	"github.com/valyala/fasthttp"
	. "gitlab.com/yeka_/pexels/log"
	. "gitlab.com/yeka_/pexels/utils"
	"go.uber.org/zap"
	"time"
)

// SearchPicture 搜索照片
func (s *Server) SearchPhoto(ctx *fasthttp.RequestCtx) {
	t1 := time.Now()
	keyword := ctx.FormValue("keyword")
	page, perPage := AnalysisPagesInfo(ctx.FormValue("page"), ctx.FormValue("per_page"))
	t2 := time.Now()
	resp, err := s.pClient.SearchPhotos(string(keyword), perPage, page)
	t3 := time.Now()
	fmt.Printf("search cost %v\n", t3.Sub(t2))
	if err != nil {
		Log().Error("pClient SearchPhotos", zap.Error(err))
		return
	}
	ctx.SetBody(Marshal2Body(resp))
	t4 := time.Now()
	fmt.Printf("total cost %v\n", t4.Sub(t1))
}

// TrendingPicture 获取热点照片
func (s *Server) TrendingPhoto(ctx *fasthttp.RequestCtx) {
	page, perPage := AnalysisPagesInfo(ctx.FormValue("page"), ctx.FormValue("per_page"))
	resp, err := s.pClient.CuratedPhotos(perPage, page)
	if err != nil {
		Log().Error("pClient CuratedPhotos", zap.Error(err))
		return
	}
	ctx.SetBody(Marshal2Body(resp))
}

// GetPicture 获取照片
func (s *Server) GetPhoto(ctx *fasthttp.RequestCtx) {
	picId, _ := ByteSlice2Int(ctx.FormValue("id"))
	resp, err := s.pClient.GetPhoto(int32(picId))
	if err != nil {
		Log().Error("pClient GetPhoto", zap.Error(err))
		return
	}
	ctx.SetBody(Marshal2Body(resp))
}

// GetRandomPicture 随机返回一张照片
func (s *Server) GetRandomPhoto(ctx *fasthttp.RequestCtx) {
	resp, err := s.pClient.GetRandomPhoto()
	if err != nil {
		Log().Error("pClient GetRandomPhoto", zap.Error(err))
		return
	}

	ctx.SetBody(Marshal2Body(resp))
}
