package service

import (
	"fmt"
	"github.com/Augustr96/gopexels"
	"github.com/buaazp/fasthttprouter"
	"log"
	"os"
)

type Server struct {
	Router  *fasthttprouter.Router
	pClient *gopexels.Client
}

func NewServer() *Server {
	s := &Server{
		Router: fasthttprouter.New(),
	}

	s.initRouter(s.Router)
	initClient(s)

	return s
}

// initRouter 注册路由
func (s *Server) initRouter(r *fasthttprouter.Router) {

	// photo
	r.GET("/photo/search", s.SearchPhoto)
	r.GET("/photo/trend", s.TrendingPhoto)
	r.GET("/photo/get", s.GetPhoto)
	r.GET("/photo/random", s.GetRandomPhoto)

	// video
	r.GET("/video/search", s.SearchVideo)
	r.GET("/video/popular", s.PopularVideo)
	r.GET("/video/random", s.GetRandomVideo)

}

// initClient ...
func initClient(s *Server) {
	initPexelsClient(s)
}

// initPexelsClient 初始化pexels api client
func initPexelsClient(s *Server) {
	token := os.Getenv("PexelsToken")
	if token == "" {
		log.Fatal("no pexles token")
	} else {
		fmt.Printf("pexels token: %s\n", token)
	}
	s.pClient = gopexels.NewClient(os.Getenv("PexelsToken"))
}
