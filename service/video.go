package service

import (
	"github.com/valyala/fasthttp"
	. "gitlab.com/yeka_/pexels/log"
	. "gitlab.com/yeka_/pexels/utils"
	"go.uber.org/zap"
)

// SearchVideo 搜索视频
func (s *Server) SearchVideo(ctx *fasthttp.RequestCtx) {
	page, perPage := AnalysisPagesInfo(ctx.FormValue("page"), ctx.FormValue("per_page"))
	resp, err := s.pClient.SearchVideo(string(ctx.FormValue("keyword")), perPage, page)
	if err != nil {
		Log().Error("pClient SearchVideo", zap.Error(err))
	}
	ctx.SetBody(Marshal2Body(resp))
}

// PopularVideo ...
func (s *Server) PopularVideo(ctx *fasthttp.RequestCtx) {
	page, perPage := AnalysisPagesInfo(ctx.FormValue("page"), ctx.FormValue("per_page"))
	resp, err := s.pClient.PopularVideo(perPage, page)
	if err != nil{
		Log().Debug("pClient PopularVideo", zap.Error(err))
	}
	ctx.SetBody(Marshal2Body(resp))
}

// GetRandomVideo ...
func (s *Server) GetRandomVideo(ctx *fasthttp.RequestCtx) {
	resp, err := s.pClient.GetRandomVideo()
	if err != nil{
		Log().Debug("pClient GetRandomPhoto", zap.Error(err))
	}
	ctx.SetBody(Marshal2Body(resp))
}