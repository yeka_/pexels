package utils

import "strconv"

func ByteSlice2Int(src []byte) (int, error) {
	return strconv.Atoi(string(src))
}
