package utils

import (
	"encoding/json"
	"fmt"
)

func Marshal2Body(src interface{}) (body []byte) {
	body, err := json.Marshal(src)
	if err != nil {
		fmt.Printf("marshal json error: %v\n", err)
		return
	}
	return
}
