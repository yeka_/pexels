package utils

const (
	DefaultPage    = 1
	DefaultPerPage = 20
)

// AnalysisPagesInfo 解析分页信息, 失败则返回默认值
func AnalysisPagesInfo(srcPage, srcPerPage []byte) (page, perPage int) {
	page, err := ByteSlice2Int(srcPage)
	if err != nil || page == 0 {
		page = DefaultPage
	}
	perPage, err = ByteSlice2Int(srcPerPage)
	if err != nil || perPage == 0 {
		perPage = DefaultPerPage
	}
	return
}
